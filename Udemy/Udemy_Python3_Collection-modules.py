# coding: utf-8
# In[1]:
##### COLLECTION MODULES #####
## Counter
from collections import Counter
# In[2]:
l = [1,1,2,2,3,3,4,4,5,5]
# In[3]:


Counter(l)


# In[4]:


# Counts the number of elements


# In[5]:


s = "MohammedRizwan"


# In[6]:


Counter(s)


# In[7]:


w = "Hai hello Yahya how are you Yahya"


# In[8]:


Counter(w)


# In[9]:


word = w.split()


# In[10]:


Counter(word)


# In[11]:


list(Counter(word))


# In[12]:


## Default dict


# In[13]:


from collections import defaultdict


# In[14]:


d = {'k1':1, 'k2':2}


# In[15]:


d['k1']


# In[16]:


d['k3']


# In[17]:


d = defaultdict(object)


# In[18]:


d['k3']
# Does not throw a key error, initializes instead


# In[19]:


d.keys()


# In[20]:


d = defaultdict(lambda: 0)


# In[21]:


d ['one']
# Takes the value that we passed while assigning defaultdict


# In[22]:


d ['two'] = 2


# In[23]:


d


# In[24]:


d.keys()


# In[25]:


d.values()


# In[26]:


## Ordered Dict
# by default dict do not have any order, as they are just mappings.
# OrderedDict - sets dict in order


# In[27]:


from collections import OrderedDict


# In[28]:


d = OrderedDict()


# In[29]:


d = {'a':1, 'b':2, 'c':3, 'd':4, 'e':5}


# In[30]:


for k, v in d.items():
    print(k,v)


# In[38]:


d1 = {}
d1['a'] = 1
d1['b'] = 2

d2 = {}
d2['b'] = 2
d2['a'] = 1


# In[39]:


print (d1 == d2)
#normal dict are equal to each other


# In[40]:


d1 = OrderedDict()
d1['a'] = 1
d1['b'] = 2

d2 = OrderedDict()
d2['b'] = 2
d2['a'] = 1


# In[41]:


print (d1 == d2)
#Ordered dict are NOT equal to each other


# In[43]:


## namedtuple
# Defining tuple index of a data


# In[44]:


from collections import namedtuple


# In[45]:


Dog = namedtuple('Dog', 'age breed name')


# In[46]:


getdog = Dog(2,'Lab','Tinker')


# In[47]:


getdog.age


# In[48]:


getdog.name


# In[49]:


getdog.breed


# In[50]:


# Otherway
getdog = Dog(age=3,breed='Dal',name='Pingo')


# In[51]:


getdog.age


# In[52]:


getdog[2]

