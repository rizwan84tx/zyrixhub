
# coding: utf-8

# In[1]:


#### Python DEBUGGER & TIMEIT ####


# In[2]:


import pdb


# In[3]:


x = [1,2]
y = 10
z = 2

result = y + z
print(result)

result2 = y + x
print (result2)


# In[4]:


x = [1,2]
y = 10
z = 2

result = y + z
print(result)

pdb.set_trace()

result2 = y + x
print (result2)


# In[5]:


##### timeit #######
# Calculates the time taken to complete a function


# In[6]:


import timeit


# In[8]:


"-".join(str(n) for n in range(100))


# In[9]:


#Calc the time to run above code
# syntax -> timeit.timeit('<code>',number=<no of time to run it>)


# In[25]:


timeit.timeit('"-".join(str(n) for n in range(100))',number=100000)


# In[26]:


timeit.timeit('"-".join([str(n) for n in range(100)])',number=100000)


# In[27]:


timeit.timeit('"-".join(map(str,range(100)))',number=100000)


# In[28]:


### above concludes that using map function is faster that other 2 methods

