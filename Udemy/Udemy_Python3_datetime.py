
# coding: utf-8

# In[1]:


#### DATETIME ####


# In[2]:


import datetime


# In[3]:


t = datetime.time(5,25,1)
# Default format datetime.time(hour,minutes,secs,micro-sec,timezone)


# In[5]:


print (t)


# In[6]:


t.hour


# In[7]:


t.minute


# In[8]:


t.second


# In[10]:


print (datetime.time)


# In[11]:


print (datetime.time.min)


# In[12]:


print (datetime.time.max)


# In[13]:


print (datetime.time.resolution)


# In[14]:


print (datetime.time.tzinfo)


# In[15]:


print (datetime.time.tzname)


# In[16]:


today = datetime.date.today()


# In[17]:


print (today)


# In[18]:


today.timetuple()


# In[19]:


##### date manipulation ####


# In[20]:


d1 = datetime.date(1985,5,26)


# In[21]:


print (d1)


# In[22]:


d2 = d1.replace(year = 2018)
print (d2)


# In[24]:


d2 - d1

